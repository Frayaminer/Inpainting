import itertools
import glob
import os

import keras
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
from scipy import misc
from skimage.transform import resize


def get_files_in_subdirs(path):
    for dirpath, _, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith('.jpg'):
                yield os.path.join(dirpath, filename)


class DataGenerator(keras.utils.Sequence):
    def __init__(self, img_path, masks_path, batch_size=2, dim=(512,512), n_channels=3, shuffle=True):
        self.dim = dim
        self.batch_size = batch_size
        self.n_channels = n_channels
        self.shuffle = shuffle

        self.img_list = glob.glob(os.path.join(img_path, '*.jpg'))
        self.masks_list = glob.glob(os.path.join(masks_path, '*.jpg'))

        self.common_len = min(len(self.img_list), len(self.masks_list))

        print('DEBUG FROM DATAGENERATOR:', self.common_len)

        self.on_epoch_end()


    def __len__(self):
        return int(np.floor(self.common_len / self.batch_size))


    def __getitem__(self, index):
        indexes = self.indexes[index*self.batch_size: (index + 1)*self.batch_size]

        img_batch_list = [self.img_list[k] for k in indexes]
        masks_batch_list = [self.masks_list[k] for k in indexes]

        X, y = self.__data_generation(img_batch_list, masks_batch_list)

        return X, y

    
    def on_epoch_end(self):
        self.indexes = np.arange(self.common_len)
        if self.shuffle:
            np.random.shuffle(self.indexes)


    def __data_generation(self, img_batch_list, masks_batch_list):
        imgs = np.empty((self.batch_size, *self.dim, self.n_channels), dtype=np.float32)
        y = np.empty_like(imgs)

        for i, name in enumerate(img_batch_list):
            img = misc.imread(name)
            img = resize(img, (*self.dim, self.n_channels))

            imgs[i,] = img
            y[i,] = img 

        masks = np.empty_like(imgs)
        for i, name in enumerate(masks_batch_list):
            mask = misc.imread(name)
            mask = resize(mask, (*self.dim, self.n_channels))

            masks[i,] = mask

        X = [imgs, masks]

        return X, y

class DataGeneratorStream(keras.utils.Sequence):
    def __init__(self, img_path, masks_path, batch_size=2, dim=(512,512), n_channels=3, shuffle=True):
        self.dim = dim
        self.batch_size = batch_size
        self.n_channels = n_channels

        # read images from subdirs 
        self.img_list = get_files_in_subdirs(img_path)
        self.masks_list = get_files_in_subdirs(masks_path)

        # very dirty hack!
        # it's a current number of masks
        self.common_len = 1312591

    def __len__(self):
        return int(np.floor(self.common_len / self.batch_size))

    def __getitem__(self, index):
        img_batch_list = itertools.islice(self.img_list, self.batch_size)
        masks_batch_list = itertools.islice(self.masks_list, self.batch_size)

        return self.__data_generation(img_batch_list, masks_batch_list)

    def __data_generation(self, img_batch_list, masks_batch_list):
        imgs = np.empty((self.batch_size, *self.dim, self.n_channels), dtype=np.float32)
        y = np.empty_like(imgs)

        for i, name in enumerate(img_batch_list):
            img = misc.imread(name)
            img = resize(img, (*self.dim, self.n_channels))

            imgs[i,] = img
            y[i,] = img 

        masks = np.empty_like(imgs)
        for i, name in enumerate(masks_batch_list):
            mask = misc.imread(name)
            mask = resize(mask, (*self.dim, self.n_channels))

            masks[i,] = mask

        X = [imgs, masks]

        return X, y
