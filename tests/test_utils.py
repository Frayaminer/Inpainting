import unittest
import numpy as np
import keras.backend as K
import utils
import tensorflow as tf

class UtilsTest(unittest.TestCase):
    def test_dilation_cross(self):
        M = np.ones((3, 3))
        M[1, 1] = 0
        P = utils.calc_dilation(M, 'cross')
        expected = np.array([[1, 0, 1], [0, 0, 0], [1, 0, 1]])
        self.assertTrue(np.array_equal(P, expected))

    def test_dilation_square(self):
        M = np.ones((3, 3))
        M[1, 1] = 0
        P = utils.calc_dilation(M, 'square')
        expected = np.zeros((3,3))
        self.assertTrue(np.array_equal(P, expected))

    def test_roll_tf(self):
        M = np.arange(1, 10).reshape((3, 3))
        shift = -1
        axis = 1
        output = K.eval(utils.roll(M, shift, axis))
        expected = np.roll(M, shift, axis)
        self.assertTrue(np.array_equal(output, expected))

    def test_roll_nowrap_left(self):
        M = np.arange(1, 10).reshape((3, 3))
        shift = -1
        axis = 1
        output = K.eval(utils.roll(M, shift, axis, wrap=False))
        expected = np.roll(M, shift, axis)
        expected[:,2] = np.zeros((3,))
        self.assertTrue(np.array_equal(output, expected))

    def test_roll_nowrap_up(self):
        M = np.arange(1, 10).reshape((3, 3))
        shift = -1
        axis = 0
        output = K.eval(utils.roll(M, shift, axis, wrap=False))
        expected = np.roll(M, shift, axis)
        expected[2,:] = np.zeros((3,))
        self.assertTrue(np.array_equal(output, expected))

    def test_gram_mat(self):
        X = np.arange(1,10, dtype=np.float32).reshape((1,3,3,1))
        G = utils.gram_mat(X)
        G_norm = K.eval(tf.norm(G, ord=1))
        expected_result = np.dot(X.reshape(3,3).T, X.reshape(3,3))
        expected_norm = K.eval(tf.norm(expected_result, ord=1))
        self.assertEqual(G_norm, expected_norm)