import unittest
import numpy as np
import keras.backend as K

try:
    import unet
    from vgg_interface import VGG16Singleton
except ImportError:  
    import sys
    sys.path.insert(0,'..')
    import unet
    from vgg_interface import VGG16Singleton

from losses import total_loss
from keras.optimizers import Adam
import tensorflow as tf
from scipy import misc
import os

class ModelTest(unittest.TestCase):
    def test_vgg(self):
        model1 = VGG16Singleton().model
        model2 = VGG16Singleton().model
        self.assertTrue(model1 is model2)

    def test_loss(self):
        test_unet = unet.UnetInpainting()
        test_unet.build_model()

        img_name = 'test_512x512.bmp'
        mask_name = 'test_512x512_mask01.bmp'
    
        try:
            data_path = './data/test_net_model/'
            image = misc.imread(os.path.join(data_path, img_name))
            mask = misc.imread(os.path.join(data_path, mask_name))
        except FileNotFoundError:
            data_path = '../data/test_net_model/'
            image = misc.imread(os.path.join(data_path, img_name))
            mask = misc.imread(os.path.join(data_path, mask_name))
        
        # Normalize mask and diplicate x3 Channel dimension 512x512 -> 512x512x3
        mask = [ [ [ int( v / 255 ), int( v / 255 ), int( v / 255 ) ] for v in r ] for r in mask ]

        # Convert into single image batch img -> [img]
        images = np.array([image])
        masks = np.array([mask])

        # images 1x512x512x3, masks 1x512x512x1
        result = test_unet.predict([images, masks])

        # TODO: here is a big problem, total_loss doesn't support batch calculation of loss!
        '''
        for i in range(result[0].shape[0]):
            y_true = tf.convert_to_tensor(images[i], dtype=tf.float32)
            I_out = tf.convert_to_tensor(result[0][i], dtype=tf.float32)
            M_out = tf.convert_to_tensor(result[1][i], dtype=tf.float32)
            print('TEST', K.eval(y_true))
            print('TEST', K.eval(I_out))
            print('TEST', K.eval(M_out))
            test_total_loss = total_loss(y_true, [I_out, M_out])
            self.assertNotEqual(K.eval(test_total_loss), 0)
        '''
        