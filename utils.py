import keras.backend as K 
from scipy.ndimage.morphology import binary_dilation
from scipy.ndimage import generate_binary_structure
import tensorflow as tf
import numpy as np

def get_activations(model, model_inputs, layer_names):
    '''
    Get activations of three first pooling layers of VGG16 net
    https://github.com/philipperemy/keras-visualize-activations/blob/master/read_activations.py
    '''
    if isinstance(model_inputs, tf.Tensor):
        # print('This is a tensor!', model_inputs)
        # TODO: evaluating the model_inputs Tensor causes a bug here! 
        # model_inputs = K.eval(model_inputs)
        pass 

    if not isinstance(model_inputs, list):
        model_inputs = [model_inputs]

    input = [model.input]
    list_inputs = model_inputs

    # input = model.input
    # list_inputs = []
    # if not isinstance(input, list):
    #     input = [input]
    #     list_inputs = [model_inputs, 0.]
    # else:
    #     list_inputs.extend(model_inputs)
    #     list_inputs.append(0.)

    outputs = [layer.output for layer in model.layers 
        if layer.name in layer_names]

    funcs = [K.function(input + [K.learning_phase()], [out]) for out in outputs]

    # activations = [func(list_inputs)[0] for func in funcs]
    activations = outputs
    return activations 


def gram_mat(X):
    """
    Calculate gram matrix as X^T * X 

    Args:
        X: A `Tensor` of shape (batch_size, h, w, c)
    
    Returns:
        A `Tensor` of shape (batch_size, w, w, c)
    """
    # Firstly, we need to change the shape of X to (b, c, h, w) to use tf.matmul 
    X = tf.transpose(X, [0,3,1,2])
    # Then, tranpose the part of X of the shape of (h, w) in each batch and channel
    X_T = tf.transpose(X, [0,1,3,2])
    G = tf.matmul(X_T, X)
    # Change channels order back from 'first' to 'last'
    G = tf.transpose(G, [0,2,3,1])
    return G


def calc_I_comp(I_out, I_gt, M):
    '''
    Compute I_comp image that is the raw output image Iout,
    but with the non-hole pixels directly set to ground truth
    '''
    return M * I_gt + (1 - M) * I_out


def calc_dilation(M, mode='cross', inverted=True):
    #print("calc_dilation")
    #print("shape M", M.shape)
    mask = 1 - M if inverted else M
    bin_struct = None
    if mode == 'cross':
        bin_struct = generate_binary_structure(2, 1).astype(np.float32)
    elif mode == 'square':
        bin_struct = generate_binary_structure(2, 2).astype(np.float32)  
    
    #print("shape bin_struct", bin_struct.shape)
    # this is a tensorflow way to stack tensor 'bin_struct' of shape(3,3) Ch times
    # to become shape of (3,3,Ch), where Ch is a size of channel dimension
    filter = tf.tile(tf.expand_dims(tf.convert_to_tensor(bin_struct), -1), [1,1,3])#tf.shape(M)[-1]])
    
    #print("shape filter", filter.shape)
    
    # the same motivation here but for batch dimension
    # filter = tf.tile(tf.expand_dims(filter, 0), [tf.shape(M)[0],1,1,1])
    # if mask is not batched add first redundant dimension
    # res = tf.cond(tf.equal(tf.rank(mask), tf.constant(3))
    #     , lambda: tf.nn.dilation2d(tf.expand_dims(mask, 0), filter, [1,1,1,1], [1,0,0,1], 'SAME')
    #     , lambda: tf.nn.dilation2d(mask, filter, [1,1,1,1], [1,0,0,1], 'SAME'))
    ##res = tf.nn.dilation2d(tf.expand_dims(mask, 0), filter, [1,1,1,1], [1,0,0,1], 'SAME')
    res = tf.nn.dilation2d(mask, filter, [1,1,1,1], [1,0,0,1], 'SAME')
    #print("shape res", res.shape)
    return 1 - res if inverted else res


def roll(a, shift, axis, wrap=True):
    #print("roll")
    #print("shape a", a.shape)
    wrapped = tf.manip.roll(a, shift, axis)
    #print("shape wrapped", wrapped.shape)
    if wrap:
        return wrapped
    else:
        zeros_shape = list(a.shape)
        #print("zeros_shape", zeros_shape)
        ones_shape = list(a.shape)
        zeros_shape[axis] = tf.Dimension(abs(shift))
        #print("zeros_shape", zeros_shape)
        ones_shape[axis] = tf.Dimension(a.shape[axis] - abs(shift))
        zeros_shape[-1] = tf.Dimension(1)
        #print("zeros_shape", zeros_shape)
        ones_shape[-1] = tf.Dimension(1)
        # zeros_shape = tuple(zeros_shape)
        # ones_shape = tuple(ones_shape)
        #print("zeros_shape", zeros_shape)
        zeros_tensor = K.zeros(zeros_shape, a.dtype)
        ones_tensor = K.ones(ones_shape, a.dtype)

        mask = K.concatenate([zeros_tensor, ones_tensor], axis=axis) if shift > 0 \
            else K.concatenate([ones_tensor, zeros_tensor], axis=axis)
        res = wrapped * mask
        #print("shape res", res.shape)
        return res


def unit_step_activation(x):
    """
    Compute unit step function of input tensor
    Differrent from original bacause expludes 0 from range of positive values 
    
    H[n] = 0, n <= 0   (instead of original n < 0)
    H[n] = 1, n >  0   (instead of original n >= 0)
    
    Inputs:
        x input Tensor, expected floating point values 0..1
    
    Output:
        ceil(x)
    """
    return tf.ceil(x)
    
def normalize(x):
    """
    Normalizes input
    X'[i] = (X[i] - min(x)) / (max(x) - min(X)) 
    
    Inputs:
        x input Tensor
    
    Outputs:
        normalized x
    """
    x_max = tf.reduce_max(x)
    x_min = tf.reduce_min(x)
    x_range = x_max - x_min
    a = tf.subtract(x, x_min)
    factor = 1.0 / x_range
    x_normalized = tf.scalar_mul(factor, a)
    return x_normalized
    
def mask_scale(input):
    """
    Scale all values in X with factor 1/(sum(M)), where M is the binary mask
    """
    x = input[0]
    m = input[1]
    
    factor = 1.0 / K.sum(m)
    x_scaled = tf.scalar_mul(factor, x)
    return x_scaled
    
def sum_channels(x):
    """
    Calculate sum over Channel dimension, keeping single-unit dimension in result tensor
    
    Inputs:
        x - input image, x.shape is (Height, Width, Channels)
        
    Outputs:
        y - output image, y.shape is (Height, Width, 1)
    
    x[i][j] = [ Rij, Gij, Bij ]  
    y[i][j] = [ Rij + Gij + Bij ]
    """
    
    y = tf.cast(x, tf.float32)
    axis = 3
    
    y = tf.reduce_sum(y, axis, keepdims=True)
    
    return y